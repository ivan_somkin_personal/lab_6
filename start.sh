#!/bin/bash

git submodule update --init --recursive

docker-compose up frontend backend db

domains='sia-test.ru,www.sia-test.ru'
email='somkin.van@yandex.ru'

docker-compose up --force-recreate -d nginx-proxy

docker-compose run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/html \
    --staging \
    --email $email \
    -d $domains\
    --agree-tos" certbot
echo 'tried getting certificate'

if [ -f "var/conf/certbot/live/sia-test.ru/fullchain.pem" ] && [ -f "var/conf/certbot/live/sia-test.ru/privkey.pem" ]; then
  echo 'got certificate'

  sed -i '/# nginx_443.conf/ r var/conf/nginx/nginx_443.conf' var/conf/nginx/nginx.conf
  sed -i 's/# nginx_443.conf/# (pasted) nginx_443.conf/' var/conf/nginx/nginx.conf
  echo 'updated nginx.conf with https'

  docker-compose up --force-recreate -d nginx-proxy
  echo 'relaunched nginx with certificate'
else
  echo 'did not get certificate, try again!'
fi